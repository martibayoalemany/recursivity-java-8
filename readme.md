### Statistics (2_000_000 iterations) 

--- Algorithms ordered by duration ---

| Desc | Speed | Memory |
|-------------|:--------------:|--------------:|
| [Adder] (TailRec) 2.000.000 |   116 ms | 1585 Kb |
| [Adder] iterate and reduce 2.000.000 |  223 ms | 7725 Kb |


| Desc | Speed | Memory |
|-------------|:--------------:|--------------:|
| [Fibonacci] iterate and reduce 2.000.000 |  101168 ms | 50687 Kb |
| [Fibonacci] (TailRec) 2.000.000 |  110381 ms | 18183 Kb |

--- Algorithms ordered by memory --- 

| Desc | Speed | Memory |
|-------------|:--------------:|--------------:|
| [Adder] (TailRec) 2.000.000 |  116 ms | 1585 Kb |
| [Adder] iterate and reduce 2.000.000 |  223 ms | 7725 Kb |

| Desc | Speed | Memory |
|-------------|:--------------:|--------------:|
| [Fibonacci] (TailRec) 2.000.000 |  110381 ms | 18183 Kb |
| [Fibonacci] iterate and reduce 2.000.000 |  101168 ms | 50687 Kb |

### Books
- The Well-Grounded Java Developer: Vital techniques of Java 7 and polyglot programming - **Benjamin J. Evans** and **Martijn Verburg**
- Functional Programming in Java: How functional techniques improve your Java programs - ** Pierre-Yves Saumont **