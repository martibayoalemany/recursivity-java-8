package com.graphai.performance;

import com.graphai.functional_patterns.*;
import com.graphai.utils.Utils;
import org.slf4j.Logger;

import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.graphai.functional_patterns.CollectionUtils.sum;
import static com.graphai.functional_patterns.CollectionUtils.sumTail;
import static com.graphai.functional_patterns.TailCall.iterate_tail;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created on 08.05.17.
 */
public class Algorithms {
    private static final Logger log;
    private static final NumberFormat nfi = NumberFormat.getNumberInstance(Locale.GERMAN);
    static {
        log = getLogger(Algorithms.class);
    }

    public static void main(String[] args) {
        Utils.printSystemProperties();
        //not_optimized_1();
        //not_optimized_2();
        Algorithms.run();
    }

    @Deprecated
    public static void not_optimized_1() {
        List<Result.Success> successes = new ArrayList<>();
        {
            // Not optimized
            int count = 2_000;
            try {

                Perf.exec_m("[Adding values] not optimized ! " + nfi.format(count), () -> {
                    List values = Stream.iterate(1, x -> x + 1).limit(count).collect(Collectors.toList());
                    // TODO: (Warning) It copies internally the list at each iteration (source: book example)
                    return sumTail(values, 0);
                }, successes);
            } catch (StackOverflowError e) {
                log.error("Stack overflow for " + count);
            }
        }
    }

    @Deprecated
    public static void not_optimized_2() {
        List<Result.Success> successes = new ArrayList<>();
        {
            int count = 2_000_000;

            Perf.exec_m("[Adding values] (TailRec) partly optimized " + count,
                    () -> {
                        Stream<BigInteger> stream = Stream.iterate(BigInteger.ONE, x -> x.add(BigInteger.ONE)).limit(count);
                        // TODO: (Warning) Instead of using an iterator instantiates the list with Collectors.toList()
                        List<BigInteger> values2 = stream.collect(Collectors.toList());
                        return iterate_tail(() -> sum(values2, BigInteger.ZERO, 0));
                    });
        }
    }

    public static void run() {
        List<Result.Success> successes = new ArrayList<>();
        NumberFormat nfi = NumberFormat.getNumberInstance(Locale.GERMAN);
        int count = 200_000;
        String count_s = nfi.format(count);
        {

            try {

                Perf.exec_m("[Adding values] iterate and reduce " + count_s, () -> {
                    return Stream.iterate(1, x -> x + 1).limit(count).reduce((a,b) -> a + b);
                }, successes);
            }
            catch(StackOverflowError e) {
                log.error("Stack overflow for " + count);
            }
        }

        {
            // Optimized, The last call of the iteration is CollectionUtils.add(count, 0)
            // The success list includes the performance information memory wise and size wise
            Perf.exec_m("[Adding values] (TailRec) " + count_s, () -> iterate_tail( () -> CollectionUtils.add(0, count)), successes);
        }

        {
            // While-loop iterator

            Perf.exec_m("[Adding values] Simple while loop and iterator " + count_s,
                    () -> {
                        Iterator<BigInteger> iterator = Stream.iterate(BigInteger.ONE, x -> x.add(BigInteger.ONE)).limit(count).iterator();
                        BigInteger value = BigInteger.ZERO;
                        while (iterator.hasNext()) {
                            System.gc();
                            value = value.add(iterator.next());
                        }

                        return value;
                    }, successes);
        }

        {
            // While-loop iterator

            Perf.exec_m("[Adding values] Simple while loop and iterator (weak-ref) " + count_s,
                    () -> {
                        Iterator<BigInteger> iterator = Stream.iterate(BigInteger.ONE, x -> x.add(BigInteger.ONE)).limit(count).iterator();
                        WeakReference<BigInteger> value = new WeakReference<>(BigInteger.ZERO);
                        while (iterator.hasNext()) {
                            value = new WeakReference<>(value.get().add(iterator.next()));
                        }

                        return value;
                    }, successes);
        }

        {
            Perf.exec_m("[Fibonacci] (TailRec) " + count_s, () -> Fibonacci.fib(count), successes);
        }

        {
            // Fibonacci iterate and reduce
            Tuple<BigInteger, BigInteger> firstValue = new Tuple<>(BigInteger.ZERO, BigInteger.ONE);

            Perf.exec_m("[Fibonacci] iterate and reduce " + count_s, () ->
                            Stream.iterate(firstValue, x -> new Tuple<>(x._2, x._1.add(x._2)))
                                    .limit(count)
                                    .reduce((a, b) -> b)
                                    .orElse(firstValue)._2
                    , successes);
        }

        System.out.println("--- Algorithms ordered by duration --- ");
        successes.stream().sorted(Comparator.comparing(Result.Success::getDuration)).forEach(System.out::println);

        System.out.println("--- Algorithms ordered by memory --- ");
        successes.stream().sorted(Comparator.comparing(Result.Success::getMemory)).forEach(System.out::println);
    }
}
