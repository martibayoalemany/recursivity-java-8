package com.graphai.performance;

public class driftEx {

    public static void main(String[] args) {
        long start_nanos = 0;
        long now_nanos= 0;
        long start_millis = System.currentTimeMillis();
        long now_millis = start_millis;

        while(start_millis == now_millis) {
            start_nanos = System.nanoTime();
            now_millis = System.currentTimeMillis();
        }

        start_millis = now_millis;
        double max_drift = 0;
        long last_millis;
        while(true) {
            last_millis = now_millis;
            // Updates the nanos as long as the millis duration is below than 1 second
            while(now_millis - last_millis < 1000) {
                now_nanos = System.nanoTime();
                now_millis = System.currentTimeMillis();
            }
            // It calculates the drift of the nanos regarding the start_nanos
            long duration_millis = now_millis - start_millis;
            double drift_nanos = 1_000_000 *
                    (((double)(now_nanos - start_nanos) / 1_000_000 ) - duration_millis);
            if(Math.abs(drift_nanos) > max_drift) {
                System.out.println("Now : " + duration_millis + " driftNanos: " + drift_nanos);
                max_drift = Math.abs(drift_nanos);
            }
        }
    }
}
