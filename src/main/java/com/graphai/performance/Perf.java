package com.graphai.performance;

import com.graphai.functional_patterns.Result;

import java.text.DecimalFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.function.Supplier;

public class Perf {
    public static <T> Result.Success<T> exec(String details, Supplier<T> supplier) {
        DecimalFormat f = new DecimalFormat("#,###.00");
        Result.Success<T> t =  exec_(details,supplier);
        System.out.printf("%s in %s ms -> %s\n",  details,
                            f.format((long)t.getDuration().toMillis()),
                            t.get());
        return t;
    }

    public static <T> Result.Success<T> exec_m(String details, Supplier<T> supplier, List<Result.Success> resultList) {
        Result.Success<T> r = exec_m(details, supplier);
        if(resultList != null)
            resultList.add(r);
        return r;
    }

    public static <T> Result.Success<T> exec_m(String details, Supplier<T> supplier) {
        DecimalFormat f = new DecimalFormat("#,###.00");
        Result.Success<T> t =  exec_(details, supplier);
        System.out.printf("%s in %s ms with memory %s Kb -> %s\n",  details,
                            f.format((long)t.getDuration().toMillis()),
                            f.format((long)t.getMemoryKb()),
                            t.get());

        return t;
    }

    private static <T> Result.Success<T> exec_(String details, Supplier<T> supplier) {
        // Release memory to ensure we get a "clean" state at the start of this method
        System.gc();
        try {
            Thread.sleep(100);
        } catch (InterruptedException y) {
            Thread.currentThread().interrupt();
        }

        Runtime r = Runtime.getRuntime();
        long memUsedBefore = r.totalMemory() - r.freeMemory();
        Instant start = Instant.now();
        T res = supplier.get();
        Instant end = Instant.now();
        long memUsedAfter = r.totalMemory() - r.freeMemory();

        return new Result.Success<T>(res, Duration.between(start,end), memUsedAfter - memUsedBefore, details);
    }


}
