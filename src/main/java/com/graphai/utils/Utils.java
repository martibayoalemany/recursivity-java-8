package com.graphai.utils;

import org.slf4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.stream.Stream;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created on 18.04.17.
 */
public class Utils {
    private static final Logger logger ;
    static {
        logger = getLogger(Utils.class);
    }

    public static void printProperties(Class clazz) {
        try {
            ClassLoader classLoader = clazz.getClassLoader();
            File file = new File(classLoader.getResource("config.properties").getFile());
            Properties prop = new Properties();
            prop.load(new FileInputStream(file));
            prop.stringPropertyNames().stream().map(s -> s + " : " + prop.get(s)).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * {@link System#getProperties() getProperties}
     */
    public static void printSystemProperties()
    {
        Enumeration it = System.getProperties().propertyNames();
        String value = "";
        StringBuffer sb = new StringBuffer();
        int size = -1;
        while(it.hasMoreElements()) {
            size++;
            value = String.valueOf(it.nextElement());
            Stream.of(value)
                    .map(s -> s + " : " + System.getProperty(s) + "\n")
                    .forEach(sb::append);
        }

        logger.info("------------------------------------------");
        logger.info("System::getProperties ( " + size + " )");
        logger.info("------------------------------------------");
        logger.info(sb.toString());
    }
}
