package com.graphai;

import com.graphai.payments.Payment;
import com.graphai.payments.PaymentList;
import com.graphai.functional_patterns.Case;
import com.graphai.functional_patterns.CollectionUtils;
import com.graphai.functional_patterns.Result;
import com.graphai.payments.Reference;
import com.graphai.performance.Algorithms;
import com.graphai.utils.Utils;
import org.slf4j.Logger;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.graphai.functional_patterns.Case.match;
import static com.graphai.functional_patterns.Case.mcase;
import static com.graphai.functional_patterns.CollectionUtils.*;
import static com.graphai.functional_patterns.Result.Failure;
import static com.graphai.functional_patterns.Result.Success;
import static org.slf4j.LoggerFactory.getLogger;

public class App implements Java9App {
    private static final Logger log;

    static {
        log = getLogger(App.class);
    }

    public static void main(String[] args) {
        // Print the java version
        PrintStream out = System.out;
        out.println(System.getProperty("java.version"));
        App app = new App();
        out.println(app.name());
        Utils.printSystemProperties();
        // Functional programming
        Function<Integer,Integer> sum = x -> x + 1;
        Function<Integer,Integer> mult = b -> b * 5;
        out.println(sum.andThen(mult).apply(2));

        // GroupBy and reduce
        final String c1Number = "2312313";
        Reference c1 = new Reference("2312313");
        Reference c2 = new Reference("231231332323");
        Map<String, List<Payment>> payments = Stream.of(new Payment(c1,2323),
                new Payment(c1,2323),
                new Payment(c2,2323))
                .map(Payment::new)
                .collect(Collectors.groupingBy(Payment::getCreditCardNumber));

        // Print all payments
        PaymentList.print(out::println, payments);

        // it combines the payments made by the same credit card
        PaymentList.combineAndPrint(out::println, payments);

        // Validation in a "functional" way
        validate(emailChecker, "username@gmail.com");
        validate(emailChecker, "non-valid");

        // Using a bind method after the checking. The bind method "visits" the corresponding method depending on the result.
        Result r = emailChecker.apply("username@gmail.com");
        r.bind( successValue -> log.info("Email correct: " + successValue),
                failureValue -> log.error("Email incorrect: " + failureValue));

        // Defining the default case with mcase
        Case.DefaultCase<String> df = mcase(() -> new Result.Success<String>("Success"));

        // If the first condition is true execute the second one
        Case<String> case1 = Case.mcase(() -> true, ()-> new Result.Success<String>("Hello world"));

        // Print the types generated
        out.println(String.format("%s - %s ", df.getClass().getName(), case1.getClass().getName()));

        // It uses a SafeVarArgs to validate several emails
        out.println(validateEmails("adsasd", "asdasdas", "username@gmail.com"));
        out.println(validateEmails("adsasd", "asdasdas", "asdfsadfsfds"));
        out.println(validateEmails("martibayoalemany@gmail.com", "asdasdas", "afdsfsadf"));
        out.println(validateEmails("adfsadfsdfs", "asdasdas", "afdsfsadf", "asdfsdfsdf", "adsfasdf", "sadfsadf", "username@gmail.com"));

        // Using a Function<T, Function<T,T>>
        // x -> y -> x + y
        List intList = list(1,2,3,4,5);
        int result = CollectionUtils.fold(intList, 0, x -> y -> x + y);
        out.println("Fold " + result);

        String res = foldLeft(intList,"0", x -> y -> "(" + x.toString() + " + " + y.toString() + ")");
        out.println(res);
        String res2 = foldRight(intList,"0", x -> y -> "(" + x.toString() + " + " + y.toString() + ")");
        out.println(res2);

        // Checking functional composition
        Function<Executable, Function<Executable, Executable>> compose =
                x -> y -> () -> {
                    x.exec();
                    y.exec();
                };

        Result.Effect<Double> printWith2decimals = x -> {
            System.out.printf("%.2f", x);
            System.out.println();
        };

        Executable program = foldLeft(list(12.232,22.323,323.232,4232.), () -> {},
                e -> d -> compose.apply(e).apply(() -> printWith2decimals.apply(d)));

        program.exec();
        Algorithms.run();
    }

    @FunctionalInterface
    interface Executable {
        void exec();
    }

    private static Pattern emailPattern = Pattern.compile("^[a-z0-9._$+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$");

    private static Function<String, Result> emailChecker = (String s) -> {
        if (s == null) {
            return new Failure("email must not be null");
        } else if (s.length() == 0) {
            return new Failure("email must not be empty");
        } else if (emailPattern.matcher(s).matches()) {
            return new Result.Success<String>(s);
        } else {
            return new Failure("email " + s + " is invalid.");
        }
    };

    private static Result<String> validateEmails(String ems1, String ems2, String ems3) {
        return match(mcase(() -> new Result.Failure("No valid mail")),
                mcase(() -> emailChecker.apply(ems1) instanceof Success , () -> new Result.Success("0 -> " + ems1)),
                mcase(() -> emailChecker.apply(ems2) instanceof Success , () -> new Result.Success("1 -> " + ems2)),
                mcase(() -> emailChecker.apply(ems3) instanceof Success , () -> new Result.Success("2 -> " + ems3))
        );
    }

    private static Result<String> validateEmails(String... ems) {
        ArrayList<Case<String>> cases = new ArrayList<>();

        int idx = 0;
        for (String em : ems) {
            final int ix = idx;
            cases.add(mcase(() -> emailChecker.apply(em) instanceof Success , () -> new Result.Success<>(ix + " -> " + em)));
            idx ++;
        }

        return match(mcase(() -> new Result.Failure<String>("No valid mail")), cases.toArray(new Case[0]));
    }

    private static void validate(Function<String, Result> checker, String s) {
        Result result = checker.apply(s);
        try {
            StringBuffer sb = new StringBuffer();
            if (result instanceof Success) {
                sb.append("Correct: ");
                if (s != null)
                    sb.append(s);
                log.info(s.toString());
            } else {
                sb.append("Error: ");
                if (s != null)
                    sb.append(s);
                log.error(s.toString());
            }
        } catch (Exception e) {
            log.error(e.toString());
        }
    }
}