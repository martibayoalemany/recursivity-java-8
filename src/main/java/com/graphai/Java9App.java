package com.graphai;


public interface Java9App {

    default String name() { return getPrivateName();}
    // Private method in interfaces are up to java 9
    private String getPrivateName() { return "private name";}
}