package com.graphai.functional_patterns;

import java.math.BigInteger;

import static com.graphai.functional_patterns.TailCall.iterate_tail;

public class Fibonacci {

    public static BigInteger fib(int x) {
        return iterate_tail( () -> fib_(BigInteger.ONE, BigInteger.ZERO,
                BigInteger.valueOf(x)));
    }

    public static TailCall<BigInteger> fib_(BigInteger acc1, BigInteger acc2, BigInteger x) {
        if (x.equals(BigInteger.ZERO)) {
            return new TailCall.Return<>(BigInteger.ZERO);
        } else if (x.equals(BigInteger.ONE)) {
            return new TailCall.Return<>(acc1.add(acc2));
        } else {
            return new TailCall.Suspend<>(() -> fib_(acc2, acc1.add(acc2), x.subtract(BigInteger.ONE)));
        }
    }
}
