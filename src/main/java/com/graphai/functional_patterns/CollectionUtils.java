package com.graphai.functional_patterns;

import javax.naming.OperationNotSupportedException;
import java.math.BigInteger;
import java.util.*;
import java.util.function.Function;

public class CollectionUtils {

    public static <T> List<T > list() {
        return Collections.emptyList();
    }

    public static <T> List<T > list(T t) {
        return Collections.singletonList(t);
    }

    public static <T> List<T > list(List<T> ts) {
        return Collections.unmodifiableList(new ArrayList<>(ts));
    }

    @SafeVarargs
    public static <T> List<T > list(T... t) {
        return Collections.unmodifiableList(Arrays.asList(Arrays.copyOf(t, t.length)));
    }

    public static <T> T head(List<T> list) {
        if (list.size() == 0) {
            throw new IllegalStateException("head of empty list");
        }
        return list.get(0);
    }

    private static <T> List<T > copy(List<T> ts) {
        return new ArrayList<>(ts);
    }

    public static <T> List<T> tail(List<T> list) {
        if (list.size() == 0) {
            throw new IllegalStateException("tail of empty list");
        }
        List<T> workList = copy(list);
        workList.remove(0);
        return Collections.unmodifiableList(workList);
    }

    public static <T> T fold(List<T> is, T identity,
                               Function<T, Function<T, T>> f) {
        T result = identity;
        for (T i : is) {
            result = f.apply(result).apply(i);
        }
        return result;
    }

    public static <T, U> U foldLeft(List<T> ts, U identity,
                                    Function<U, Function<T, U>> f) {
        U result = identity;
        for (T t : ts) {
            result = f.apply(result).apply(t);
        }
        return result;
    }

    public static <T, U> U foldLeft2(List<T> ts, U identity,
                                    Function<U, Function<T, U>> f) {
        TailCall<U> ret = foldLeft_(ts, identity, f);
        while(ret.isSuspend())
            ret = ret.resume();
        return ret.eval();
    }

    private static <T, U> TailCall<U> foldLeft_(List<T> ts, U identity,
                                                Function<U, Function<T, U>> f) {
        return ts.isEmpty()
                ? new TailCall.Return<U>(identity)
                : new TailCall.Suspend<U>(() -> foldLeft_(tail(ts),
                f.apply(identity).apply(head(ts)), f));
    }

    public static <T, U> U foldRight(List<T> ts, U identity,
                                     Function<T, Function<U, U>> f) {
        U result = identity;
        for (int i = ts.size(); i > 0; i--) {
            result = f.apply(ts.get(i - 1)).apply(result);
        }
        return result;
    }

    public static <T> List<T> cloneReverse(List<T> list) {
        List<T> result = new ArrayList<T>();
        for(int i = list.size() - 1; i >= 0; i--) {
            result.add(list.get(i));
        }

        return Collections.unmodifiableList(result);
    }

    public static Integer sum(List<Integer> list) {
        return sumTail(list, 0);
    }

    public static Integer sumTail(List<Integer> list, int acc) {
        return list.isEmpty()
                ? acc
                : sumTail(tail(list), acc + head(list));
    }

    public static TailCall<Integer> add(int x, int y) {
        return y == 0
                ? new TailCall.Return<>(x)
                : new TailCall.Suspend<>(() -> add(x + 1, y-1));
    }

    public static TailCall<Integer> sum(List<Integer> list, Integer acc, Integer idx) {
        return list.size() == idx
                ? new TailCall.Return<Integer>(acc)
                : new TailCall.Suspend<Integer>(() -> sum(list, acc + list.get(idx)  , idx + 1));
    }

    public static TailCall<BigInteger> sum(Iterator<BigInteger> iterator, BigInteger acc) {
        return (!iterator.hasNext())
                ? new TailCall.Return<BigInteger>(acc)
                : new TailCall.Suspend<BigInteger>(() -> sum(iterator, acc.add(iterator.next())));
    }

    public static TailCall<BigInteger> sum(List<BigInteger> list, BigInteger acc, int idx) {
        return (list.size() == idx)
                ? new TailCall.Return<BigInteger>(acc)
                : new TailCall.Suspend<BigInteger>(() -> sum(list, acc.add(list.get(idx)), idx + 1));
    }

    public static TailCall<Integer> sum(WrapSupplier<Integer> supplier, Integer acc, Integer idx) {
        return supplier.hasEnded(idx) ?
                 new TailCall.Return<Integer>(acc)
                : new TailCall.Suspend<Integer>(() -> sum(supplier, acc + supplier.get(idx)  , idx + 1));
    }

    public static TailCall<BigInteger> sum(WrapSupplier<BigInteger> supplier, BigInteger acc, Integer idx) {
        return supplier.hasEnded(idx) ?
                new TailCall.Return<BigInteger>(acc)
                : new TailCall.Suspend<BigInteger>(() -> sum(supplier, acc.add(supplier.get(idx))  , idx + 1));
    }

    public static class WrapSupplier<T> {

        private List<T> list;

        public WrapSupplier(List<T> list) {
            this.list = list;
        }

        public T get(int idx) {
            return list.get(idx);
        }

        public boolean hasEnded(int idx) {
            return list.size() <= idx;
        }
    }

}
