package com.graphai.functional_patterns;

import java.time.Duration;
import java.util.function.Supplier;

public interface Result<T> {

    interface Effect<T>  {
        void apply(T t);
    }

    void bind(Effect<T> success, Effect<String> failure);

    String getMessage();

    class Success<T> implements Result<T>, Supplier<T> {
        private final T value;
        private final Duration duration;
        private final long memory;
        private final String message;

        public Success(T t) {
            this.value = t;
            this.duration = Duration.ofMillis(0);
            this.memory = 0;
            this.message = "";
        }

        public Success(T t, Duration duration) {
            this.value = t;
            this.duration = duration;
            this.memory = 0;
            this.message = "";
        }

        public Success(T t, Duration duration, long memory, String message) {
            this.value = t;
            this.duration = duration;
            this.memory = memory;
            this.message = message;
        }

        @Override
        public T get() {
            return value;
        }

        @Override
        public void bind(Effect<T> success, Effect<String> failure) {
            success.apply(value);
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String toString() {
            return toMdRow();
            //return String.format("%s in %d ms with %d Kb result %s", getMessage(), getDuration().toMillis(), getMemoryKb(), get());
        }

        public String toMdRow() {
            return String.format("| %s |  %d ms | %d Kb |", getMessage(), getDuration().toMillis(), getMemoryKb());
        }

        public Duration getDuration() {
            return duration;
        }

        public long getMemory() {
            return memory;
        }
        public long getMemoryKb() {
            return memory / 1_000;
        }

        public long getMemoryMb() {
            return memory / 1_000_000;
        }
    }

    class Failure<T> implements Result {
        private final String errorMessage;
        public Failure(String s) {
            this.errorMessage = s;
        }

        public String getMessage() {
            return errorMessage;
        }

        @Override
        public void bind(Effect success, Effect failure) {
            failure.apply(errorMessage);
        }

        @Override
        public String toString() {
            return getMessage();
        }
    }
}