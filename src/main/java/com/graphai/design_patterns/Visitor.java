package com.graphai.design_patterns;


import java.util.UUID;
import java.util.function.Consumer;

interface IVisitor {
    void visit(Target target);
}

class Visitor implements IVisitor {

    @Override
    public void visit(Target target) {
        target.setColor(target.getColor() == Color.RED ? Color.BLUE : Color.RED);
        target.setName(UUID.randomUUID().toString());
    }
}

enum Color {
    RED(0),
    BLUE(1);

    private final int value;
    Color(int value) {
        this.value = value;
    }

};

class Target implements Consumer<IVisitor> {
    private Color color;
    private String name;

    Target(Color color, String name) {
        this.color = color;
        this.name = name;
    }

    @Override
    public void accept(IVisitor iVisitor) {
        iVisitor.visit(this);
    }

    @Override
    public Consumer<IVisitor> andThen(Consumer<? super IVisitor> consumer) {
        return null;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class VisitorEx {
    public static void main(String[] params) {
        Target target = new Target(Color.RED, "<none>");
        Visitor visitor = new Visitor();

        for(int i = 0; i < 20; i++) {
            target.accept(visitor);
            System.out.println(" " + target.getColor() + " " + target.getName());
        }
    }
}
