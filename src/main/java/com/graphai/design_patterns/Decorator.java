package com.graphai.design_patterns;

abstract class Component {
    public abstract void doJob();
}

class ConcreteComponent extends Component {
    @Override
    public void doJob() {
        System.out.println("ConcreteComponent");
    }
}

abstract class AbstractDecorator extends Component {
    protected Component com;
    public void setCom(Component com) {
        this.com = com;
    }

    @Override
    public void doJob() {
        if(com!= null)
            com.doJob();
    }
}

class ConcreteDecorator extends AbstractDecorator {

    public void doJob() {
        super.doJob();
        System.out.println(this.getClass().getName());
    }
}

class ConcreteDecorator2 extends AbstractDecorator {

    public void doJob() {
        super.doJob();
        System.out.println(this.getClass().getName());
    }
}

class Decorator {
    public static void main(String[] args) {
        ConcreteComponent com = new ConcreteComponent();
        ConcreteDecorator dec = new ConcreteDecorator();
        ConcreteDecorator2 dec2 = new ConcreteDecorator2();
        dec.doJob();
        dec2.doJob();
        dec.setCom(com);
        dec2.setCom(com);
        dec.doJob();
        dec2.doJob();
    }
}