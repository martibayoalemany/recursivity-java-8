package com.graphai.design_patterns;

class Car implements Cloneable {
    private String brand;
    private int doors;

    Car(String brand, int doors) {
        this.brand = brand;
        this.doors = doors;
    }

    @Override
    public Car clone() throws CloneNotSupportedException {
        return (Car)super.clone();
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }
}

class Bus {
    private String brand;

    Bus(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}

public class Prototype {


    public static void main(String[] args) throws CloneNotSupportedException {
        Car a = new Car("BMW",4);
        Car b = a.clone();
        b.setBrand("VW");
        System.out.println(b.getBrand() + " " + b.getDoors());
        System.out.println(a.getBrand() + " " + a.getDoors());

        Bus c = new Bus("VW");
        Bus d = c;
        System.out.println(System.identityHashCode(c) + " " + System.identityHashCode(d));
    }
}
