package com.graphai.design_patterns;

import java.util.ArrayList;
import java.util.List;

class Observer {
   void update(ISubject subject) {
       Subject inst = (Subject) subject;
       System.out.println("Observer observed flag " + inst.getFlag());
   }
}

interface ISubject {
    void register(Observer o);
    void unregister(Observer o);
    void notifyObservers();
}

class Subject implements ISubject {
    private List<Observer> observerList = new ArrayList<Observer>();
    private int flag;

    @Override
    public void register(Observer o) {
        observerList.add(o);
    }

    @Override
    public void unregister(Observer o) {
        observerList.remove(o);
    }

    @Override
    public void notifyObservers() {
        for(Observer obs : observerList)
            obs.update(this);
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
        notifyObservers();
    }
}

class ObserverMain {
    public static void main(String[] args) {
        Observer o1 = new Observer();
        Subject sub = new Subject();
        sub.register(o1);
        sub.setFlag(5);
        sub.setFlag(25);
        sub.unregister(o1);
        sub.setFlag(26);

    }
}