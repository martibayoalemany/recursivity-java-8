package com.graphai.design_patterns;

import org.slf4j.Logger;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Supplier;

import static org.slf4j.LoggerFactory.getLogger;

interface ISingleton {
    static ISingleton getInst() {
        return null;
    }
}

class Singleton implements  ISingleton{
    private static final Logger log;

    static {
        log = getLogger(Singleton.class);
    }

    public static ISingleton inst = null;
    private int value;

    protected Singleton() {
        this.value = new Random().nextInt(1000);
        StringBuffer sb = new StringBuffer();
        sb.append("Constructor ");
        sb.append(" " +  System.identityHashCode(this));
        sb.append(" " + this.value + " ");
        sb.append((inst != null) ? " inst not null " : " inst null ");
        log.info(sb.toString());
    }


    public static ISingleton getInst() {
        if (inst == null) {
            long start = System.currentTimeMillis();
            log.info("Singleton new start ");
            ISingleton tmp = new Singleton();
            long end = System.currentTimeMillis();
            log.debug(String.format("Singleton new finished  %d %d ",   System.identityHashCode(tmp),  end-start));
            if(inst != null) {
                log.error(String.format("Synchronization error: Singleton was already created"));
                return inst;
            }

            inst = tmp;
        }

        return inst;
    }

    public int getValue() {
        return value;
    }
}

class Singleton2 implements ISingleton
{
    private Singleton2() {}

    private static class SingletonHelper {
        private static final Singleton2 inst = new Singleton2();
    }

    public static Singleton2 getInst() {
        return SingletonHelper.inst;
    }
}

class SingletonPatternEx {

    private static final Logger log;

    static {
        log = getLogger(SingletonPatternEx.class);
    }


    public static void main(String[] args) {

        System.out.println("-------- Nested static class ------------------------");
        getMultipleInstances(() -> Singleton2.getInst());

        System.out.println("-------- No Synchronization ------------------------");
        getMultipleInstances(() -> Singleton.getInst());
        Singleton.inst = null;

        System.out.println("--------- Reflection, static call and dynamic -----------------------");
        getMultipleInstances(() -> getDynInst(Singleton.class).get());
        Singleton.inst = null;

        System.out.println("--------- Reflection and static call -----------------------");
        getMultipleInstances(() -> getInst(Singleton.class, true).get());
        Singleton.inst = null;

        System.out.println("---------- Reflection and non static ----------------------");
        getMultipleInstances(() -> getInst(Singleton.class, false).get());
        Singleton.inst = null;
    }

    private static void getMultipleInstances(Supplier<ISingleton> supplier) {
        ArrayList<Thread> threads = new ArrayList<Thread>();
        for(int i = 0; i < 10; i++) {
            threads.add(
                    new Thread(() -> {
                        ISingleton inst = supplier.get();
                        log.info("HC : " + System.identityHashCode(inst) + " " + inst.getClass());
                    })
            );
        }

        log.debug("ForkJoinPool size: " + ForkJoinPool.commonPool().getPoolSize());

        threads.forEach( t -> t.start());
        threads.forEach(t -> {
            try {
                t.join();
                log.info("Joined " + t.getId() + " " + t.getState().name());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    private static Supplier<ISingleton> getInst(Class clazz, boolean staticCall) {
        return () -> {
            try {
                Object instance = clazz.newInstance();
                Method m = instance.getClass().getMethod("getInst");
                return (Singleton) (staticCall ? m.invoke(null) : m.invoke(instance));
            } catch (IllegalAccessException | InvocationTargetException |
                    NoSuchMethodException | InstantiationException e) {
                e.printStackTrace();
            }
            return null;
        };
    }

    private static Supplier<ISingleton> getDynInst(Class clazz) {
        return () -> {
            MethodHandle mh = null;
            MethodType mt = MethodType.methodType(ISingleton.class);
            MethodHandles.Lookup lk = MethodHandles.lookup();

            try {
                mh = lk.findStatic(clazz, "getInst", mt);
                return (ISingleton)mh.invoke();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            return null;
        };
    }


}