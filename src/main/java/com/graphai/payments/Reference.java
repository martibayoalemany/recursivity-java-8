package com.graphai.payments;

public class Reference {
    private final String number;

    public Reference(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }
}
