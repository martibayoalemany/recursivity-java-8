package com.graphai.payments;


import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

public class PaymentList {
    /**
     * It prints all strings by giving the consumer one string at a time
     * @param consumer
     * @param payments
     */
    public static void print(Consumer<String> consumer, Map<String, List<Payment>> payments) {
        for(String key : payments.keySet()) {
            for(Payment p : payments.get(key)) {
                String str = String.format("\tCredit card number: %s - Payment: %s", key, p.toString());
                consumer.accept(str);
            }
        }
    }

    public static void combineAndPrint(Consumer<String> consumer, Map<String, List<Payment>> payments) {
        for(String key : payments.keySet()) {
            Optional<Payment> p = payments.get(key).stream().reduce((Payment a, Payment b) -> a.combine(b));
            String str = String.format("\tCredit card number: %s - Payment: %s", key, p.get().toString());
            consumer.accept(str);
        }
    }
}
