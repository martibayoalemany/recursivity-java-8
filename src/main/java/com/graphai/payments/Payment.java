package com.graphai.payments;

public class Payment {
    public final Reference reference;
    public final int amount;

    public Payment(Payment p) {
        reference = p.reference;
        amount = p.amount;
    }

    public Payment(Reference reference, int amount) {
        this.reference = reference;
        this.amount = amount;
    }

    public Payment combine(Payment payment) {
        if(reference.equals(payment.reference))
            return new Payment(reference, amount + payment.amount);

        throw new IllegalStateException("Cards don't match");
    }

    public String getCreditCardNumber() {
        return reference.getNumber();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Number: ").append(reference.getNumber()).append(" Amount: ").append(amount);
        return sb.toString();
    }
}
