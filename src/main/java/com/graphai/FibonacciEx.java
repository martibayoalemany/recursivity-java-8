package com.graphai;

import com.graphai.performance.Perf;
import com.graphai.functional_patterns.Tuple;
import com.graphai.utils.Utils;

import java.math.BigInteger;
import java.util.stream.Stream;

/**
 * Created on 10.04.17.
 */
public class FibonacciEx {

    public static void main(String[] args) {

        Utils.printSystemProperties();
        Utils.printProperties(FibonacciEx.class);
        Utils.printProperties(App.class);

        {
            int count = 200_000;
            Perf.exec_m("Fibonacci (Rec_tail) optimized " + count, () -> com.graphai.functional_patterns.Fibonacci.fib(count));
        }

        {
            // Fibonacci iterate and reduce
            int count = 200_000_000;
            Tuple<BigInteger, BigInteger> firstValue = new Tuple<>(BigInteger.ZERO, BigInteger.ONE);

            Perf.exec_m("Fibonacci iterate and reduce " + count, () ->
                    Stream.iterate(firstValue, x -> new Tuple<>(x._2, x._1.add(x._2)))
                            .limit(count)
                            .reduce((a, b) -> b)
                            .orElse(firstValue)._2
            );
        }
    }
}
