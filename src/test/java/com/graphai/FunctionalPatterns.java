package com.graphai;


import com.graphai.performance.Perf;
import com.graphai.functional_patterns.Result;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FunctionalPatterns {

    @Test
    public void testPerf() {
        Result.Success<List<Integer>> res = Perf.exec("testPerf", () ->
                Stream.iterate(0, i -> i + 1).limit(1000).collect(Collectors.toList()));
        Assert.assertNotNull(res.get());
        Assert.assertNotNull(res.getDuration());
        Assert.assertEquals(1000, res.get().size());
    }
}
