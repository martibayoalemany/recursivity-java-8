package com.graphai;
import com.graphai.functional_patterns.Fibonacci;
import org.junit.Test;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import static org.junit.Assert.assertNotNull;

public class AppTest {
    @Test
    public void testAppHasAGreeting() {
        com.graphai.App classUnderTest = new com.graphai.App();
        assertNotNull("app should have a name", classUnderTest.name());
    }

    @Benchmark
    public void benchmark_Fibonacci() {
        Fibonacci.fib(2_000);
        Fibonacci.fib(20_000);
        Fibonacci.fib(200_000);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(AppTest.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
